#! /bin/bash
set -x

unzip /tmp/consul.zip -d /usr/bin/
unzip /tmp/vault.zip -d /usr/bin/

useradd consul --no-create-home
useradd vault --no-create-home

mkdir -p /etc/vault/ssl/private/
mkdir -p /etc/vault/ssl/certs/
mkdir -p /etc/consul/conf.d
chown consul /etc/consul/conf.d
chown consul /etc/consul
mkdir /var/consul
chown consul /var/consul
mkdir /etc/consul/scripts
chown consul /etc/consul/scripts
mkdir /etc/vault
chown vault /etc/vault
mkdir /var/lib/vault
chown vault /var/lib/vault
mkdir /var/lib/consul
chown consul /var/lib/consul

mv /tmp/vault.key /etc/vault/ssl/private/vault.key
mv /tmp/vault.crt /etc/vault/ssl/certs/vault.crt

sudo cp /etc/vault/ssl/certs/vault.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates --fresh

cat > "/etc/systemd/system/vault.service" <<EOF
[Unit]
Description=vault
Wants=network.target
After=network.target

[Service]
Environment="PATH=/usr/local/bin:/usr/bin:/bin"
RuntimeDirectory=vault
ExecStart=/usr/bin/vault server -config=/etc/vault/config.json -log-level=info
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=TERM
User=vault
WorkingDirectory=/var/lib/vault

[Install]
WantedBy=multi-user.target
EOF

cat > "/etc/vault/config.json" <<EOF
{
  "disable_mlock": true,
  "listener": {
    "tcp": {
      "address": "$(hostname -i):8200",
      "cluster_address": "$(hostname -i):8201",
      "tls_cert_file": "/etc/vault/ssl/certs/vault.crt",
      "tls_key_file": "/etc/vault/ssl/private/vault.key",
      "tls_disable": false
    },
  },
  "listener": {
    "tcp": {
      "address": "127.0.0.1:8200",
      "tls_cert_file": "/etc/vault/ssl/certs/vault.crt",
      "tls_key_file": "/etc/vault/ssl/private/vault.key",
      "tls_disable": false
    }
  },
  "storage": {
   "consul": {
        "address": "127.0.0.1:8500", "path": "vault/"
    }
  },
  "api_addr": "http://$(hostname -i):8200",
  "telemetry": {
    "prometheus_retention_time": "30s",
    "disable_hostname": true
  }
}
EOF

cat > "/etc/systemd/system/consul.service" <<EOF
[Unit]
Description=consul
Wants=network.target
After=network.target

[Service]
Environment="GOMAXPROCS=2" "PATH=/usr/local/bin:/usr/bin:/bin"
ExecStart=/usr/bin/consul agent -config-file=/etc/consul/consul.json -config-dir=/etc/consul/conf.d
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=TERM
User=consul
WorkingDirectory=/var/lib/consul

[Install]
WantedBy=multi-user.target
EOF

cat > "/etc/consul/consul.json" <<EOF
{
  "server": false,
  "datacenter": "dc1",
  "node_name": "$(hostname)",
  "data_dir": "/var/consul/data",
  "bind_addr": "$(hostname -i)",
  "client_addr": "127.0.0.1",
  "retry_join": ["jcampbell-consul-1","jcampbell-consul-2","jcampbell-consul-3","jcampbell-consul-4","jcampbell-consul-5"],
  "log_level": "DEBUG",
  "enable_syslog": true,
  "acl_enforce_version_8": false,
  "telemetry": {
    "prometheus_retention_time": "30s",
    "disable_hostname": true
  }
}
EOF

systemctl daemon-reload
systemctl start consul vault
sleep 10
systemctl status --no-pager consul vault
exit
