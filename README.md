# Vault Clustser

## Description

Hello, This repo was created as a template for spinning up your own HA Vault and Consul instances in GCE. To use this repo, clone it into a local directory, create a terraform.tfvars file where you have the following variables declared:

```
.
├── README.md
├── Vault_consul_dashboard.json
├── consul.tf
├── consulserver.sh
├── grafanaserver.sh
├── main.tf
├── networks.tf
├── prometheus.tf
├── prometheusserver.sh
├── statsd_exporter
│   └── statsdexporter.sh
├── terraform.tfstate
├── terraform.tfstate.backup
├── terraform.tfvars
├── variables.tf
├── vault.tf
└── vaultserver.sh
```

## Variables

• region

• gcp_project

• name (any name you want, this will be the prefix for each instane)

• machine type (this is the type of machine instance that will be created *)

• source image (1)

• network_name

• consul_cluster_name

• vault_cluster_name

• zone

• credentials (2)

• ssh_key (3)

(1) These vaules can be found here: https://cloud.google.com/compute/docs The source image for the vault module should be any version of Ubuntu 16.04

(2) Credentials can be created on the API section for the project in GCE consul

(3) ssh_key can be located in your ~/.ssh/ dir after you have set up the gcloud sdk on your local system


Once completed, run terraform init and terraform plan in the repo dir and if everything goes correctly, run terraform apply.

In addition to the Vault and Consul cluster. This terraform deployment will also create a Prometheus/Grafan node.
